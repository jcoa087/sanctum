<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;

Route::get('/users', function () {
    $user = User::findOrFail(1);
    return response()->json($user);
});
